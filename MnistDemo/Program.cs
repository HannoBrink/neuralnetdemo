﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using ConvNetSharp.Core;
using ConvNetSharp.Core.Layers.Double;
using ConvNetSharp.Core.Serialization;
using ConvNetSharp.Core.Training;
using ConvNetSharp.Volume;
using ConvNetSharp.Volume.Double;

namespace CnnDemo
{
    public class CNNHelper
    {
        private readonly CircularBuffer<double> _testAccWindow = new CircularBuffer<double>(100);
        private readonly CircularBuffer<double> _trainAccWindow = new CircularBuffer<double>(100);
        private Net<double> _net;
        private int _stepCount;
        private SgdTrainer<double> _trainer;

        public void InitializeNeuralNetwork()
        {
            this._net = new Net<double>();
            this._net.AddLayer(new InputLayer(28, 28, 1));
            this._net.AddLayer(new ConvLayer(5, 5, 8) { Stride = 1, Pad = 2 });
            this._net.AddLayer(new ReluLayer());
            this._net.AddLayer(new PoolLayer(2, 2) { Stride = 2 });
            this._net.AddLayer(new ConvLayer(5, 5, 16) { Stride = 1, Pad = 2 });
            this._net.AddLayer(new ReluLayer());
            this._net.AddLayer(new PoolLayer(3, 3) { Stride = 3 });
            this._net.AddLayer(new FullyConnLayer(10));
            this._net.AddLayer(new SoftmaxLayer(10));

            this._trainer = new SgdTrainer<double>(this._net)
            {
                LearningRate = 0.01,
                BatchSize = 20,
                L2Decay = 0.001,
                Momentum = 0.9
            };

        }

        public double TrainOnData(int numberOfSamples = 1000)
        {
            var datasets = new DataSets();
            if (!datasets.Load(numberOfSamples))
            {
                return -10000;
            }

            for (int i = 0; i < numberOfSamples; i++)
            {

                var trainSample = datasets.Train.NextBatch(this._trainer.BatchSize);
                Train(trainSample.Item1, trainSample.Item2, trainSample.Item3);

                var testSample = datasets.Test.NextBatch(this._trainer.BatchSize);
                Test(testSample.Item1, testSample.Item3, this._testAccWindow);

                Console.WriteLine("Loss: {0} Train accuracy: {1}% Test accuracy: {2}%", this._trainer.Loss,
                    Math.Round(this._trainAccWindow.Items.Average() * 100.0, 2),
                    Math.Round(this._testAccWindow.Items.Average() * 100.0, 2));

                Console.WriteLine("Example seen: {0} Fwd: {1}ms Bckw: {2}ms", this._stepCount,
                    Math.Round(this._trainer.ForwardTimeMs, 2),
                    Math.Round(this._trainer.BackwardTimeMs, 2));
            }

            return Math.Round(this._testAccWindow.Items.Average() * 100.0, 2);
        }

        private void Test(Volume<double> x, int[] labels, CircularBuffer<double> accuracy, bool forward = true)
        {
            if (forward)
            {
                this._net.Forward(x);
            }

            var prediction = this._net.GetPrediction();

            for (var i = 0; i < labels.Length; i++)
            {
                accuracy.Add(labels[i] == prediction[i] ? 1.0 : 0.0);
            }
        }

        private void Train(Volume<double> x, Volume<double> y, int[] labels)
        {
            this._trainer.Train(x, y);

            Test(x, labels, this._trainAccWindow, false);

            this._stepCount += labels.Length;
        }

        public int getPrediction(Bitmap image)
        {
            var imageVolume = convertImageToVolume(image);

            this._net.Forward(imageVolume);
            var predictionList = this._net.GetPrediction();

            //var maxProbability = predictionList.Max();
            //var predictedClass = Array.IndexOf(predictionList, maxProbability);
            var predictedClass = predictionList[0];

            return predictedClass;
        }

        private Volume<double> convertImageToVolume(Bitmap image)
        {
            var inputHeight = 28;
            var inputWidth = 28;
            var scaledImage = new Bitmap(image, new Size(inputHeight, inputWidth));

            var shape = new Shape(inputHeight, inputWidth, 1, 1);
            var rawData = new double[inputHeight * inputWidth * 1];

            for (int x = 0; x < inputWidth; x++)
            {
                double[] row = new double[inputHeight];
                for (int y = 0; y < inputHeight; y++)
                {
                    var val = image.GetPixel(x, y).GetBrightness();
                    rawData[(x * inputHeight) + y] = val;
                }
 
            }
            var dataVolume = BuilderInstance.Volume.From(rawData, shape);

            return dataVolume;
        }

        public void Save()
        {
            File.WriteAllText(@"net.json", _net.ToJson());
        }

        public void Load(string path)
        {
            var json = File.ReadAllText(path);
            _net = SerializationExtensions.FromJson<double>(json);
        }
    }
}