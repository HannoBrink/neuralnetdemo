﻿using CnnDemo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandwritingRecognition
{
    public partial class frmCnnDemo : Form
    {
        bool isMouseDown = new Boolean();
        Point lastPoint = Point.Empty;
        CNNHelper CnnHelper = new CNNHelper();

        public frmCnnDemo()
        {
            InitializeComponent();

            //Create image
            Bitmap bmp = new Bitmap(pbInput.Width, pbInput.Height);
            pbInput.Image = bmp;

            //Create neural net
            CnnHelper = new CNNHelper();
            CnnHelper.InitializeNeuralNetwork();
        }

        private void pbInput_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = e.Location;
            isMouseDown = true;
        }

        private void pbInput_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
            lastPoint = Point.Empty;
        }

        private void pbInput_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown == true)
            {
                if (lastPoint != null)

                {
                    if (pbInput.Image == null)
                    {
                        Bitmap bmp = new Bitmap(pbInput.Width, pbInput.Height);
                        pbInput.Image = bmp; 
                    }
                    using (Graphics g = Graphics.FromImage(pbInput.Image))
                    {

                        g.FillEllipse(new SolidBrush(Color.White), e.Location.X, e.Location.Y, 10, 10);
                        g.SmoothingMode = SmoothingMode.Default;
                    }
                    pbInput.Invalidate();
                    lastPoint = e.Location;
                }

            }

        }

        private void btnTrain_Click(object sender, EventArgs e)
        {
            
            var accuracy = CnnHelper.TrainOnData(5000);
            lblPrediction.Text = Convert.ToString(accuracy);
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            var prediction = CnnHelper.getPrediction(new Bitmap(pbInput.Image, 28, 28));
            lblPrediction.Text = Convert.ToString(prediction);

            Bitmap bmp = new Bitmap(pbInput.Width, pbInput.Height);
            pbInput.Image = bmp;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CnnHelper.Save();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            CnnHelper.Load("net.json");
        }
    }
}
